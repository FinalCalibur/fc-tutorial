	package scripts.fc.api.login;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Keyboard;
import org.tribot.api.input.Mouse;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Login;
import org.tribot.api2007.Login.STATE;
import org.tribot.api2007.Screen;

public class FCLogin
{
	private final static Point WELCOME_TO_RUNESCAPE = new Point(310, 242);
	private final static Point WORLD_ONE = new Point(217, 59);
	private final static Point INVALID = new Point(283, 192);
	private final static Point WORLD_SELECTION_EXIT = new Point(734, 14);
	private final static Rectangle EXISTING_USER = new Rectangle(397, 276, 129, 28);
	private final static Rectangle TRY_AGAIN = new Rectangle(317, 264, 131, 25);
	private final static Rectangle CANCEL = new Rectangle(398, 307, 128, 27);
	//private final static Rectangle LOGIN = new Rectangle(241, 308, 125 ,23);
	
	private final static Condition WELCOME_SCREEN = isLoggedIn();
	
	
	public static boolean login(String username, String password)
	{
		if(Login.getLoginState() == STATE.INGAME)
			return true;
		else if(Login.getLoginState() == STATE.WELCOMESCREEN)
			return Login.login();
		
		while(isInvalid())
		{
			General.println("Clicking try again");
			Mouse.clickBox(TRY_AGAIN, 1);
			General.sleep(600);
			Mouse.clickBox(CANCEL, 1);
			General.sleep(600);
		}
		
		while(isWorldSelectionOpen())
		{
			General.println("Closing world selection");
			Mouse.click(WORLD_SELECTION_EXIT, 1);
			General.sleep(600);
		}
		
		while(isWelcomeToRunescape())
		{
			General.println("Clicking existing user");
			Mouse.clickBox(EXISTING_USER, 1);
			General.sleep(600);
		}
		
		General.println("Type username");
		Keyboard.typeSend(username);
		
		General.println("Type password");
		Keyboard.typeSend(password);
		
		if(Timing.waitCondition(WELCOME_SCREEN, 7500))
		{
			General.println("Successfully logged in");
			return Login.login();
		}
		
		
		return false;
	}
	
	public static boolean isWelcomeToRunescape()
	{
		return Screen.getColorAt(WELCOME_TO_RUNESCAPE).equals(Color.YELLOW);
	}
	
	public static boolean isInvalid()
	{
		return Screen.getColorAt(INVALID).equals(Color.YELLOW);
	}
	
	private static boolean isWorldSelectionOpen()
	{
		return Screen.getColorAt(WORLD_ONE).equals(Color.BLACK);
	}
	
	private static Condition isLoggedIn()
	{
		return new Condition()
		{
			public boolean active()
			{
				General.sleep(100);
				return Login.getLoginState() == STATE.WELCOMESCREEN;
			}
		};
	}
}

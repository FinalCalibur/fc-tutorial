package scripts.fc.missions.fctutorial.tasks.runescape_guide;

import org.tribot.api.Timing;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.objects.ClickObject;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class RunescapeGuideDoor extends Task
{
	private static final long serialVersionUID = 3157225351510368011L;

	@Override
	public void execute()
	{
		if(new ClickObject("Open", "Door", new RSArea(new RSTile(3097, 3107, 0), 5)).execute())
			Timing.waitCondition(FCConditions.settingNotEqualsCondition(FCTutorial.PROGRESS_SETTING, 10), 2000);
	}

	@Override
	public boolean shouldExecute()
	{
		return FCTutorial.getProgress() == 10;
	}

	@Override
	public String getStatus()
	{
		return "Click door";
	}

}

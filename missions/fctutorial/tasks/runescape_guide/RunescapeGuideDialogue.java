package scripts.fc.missions.fctutorial.tasks.runescape_guide;

import org.tribot.api2007.Game;

import scripts.fc.api.interaction.impl.npcs.NpcDialogue;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class RunescapeGuideDialogue extends Task
{
	private static final long serialVersionUID = -7875730470627555141L;

	@Override
	public void execute()
	{
		new NpcDialogue("Talk-to", "Runescape Guide", 15, 0).execute();
	}

	@Override
	public boolean shouldExecute()
	{
		return (FCTutorial.getProgress() == 0 && Game.getSetting(22) > 0)
				|| FCTutorial.getProgress() == 7;
	}

	@Override
	public String getStatus()
	{
		return "Runescape Guide Dialogue";
	}

}

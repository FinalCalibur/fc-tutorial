package scripts.fc.missions.fctutorial.tasks.quest_guide;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Camera;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.objects.ClickObject;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class OpenQuestGuideDoor extends Task
{
	private static final long serialVersionUID = 7902124529325875281L;

	@Override
	public void execute()
	{
		if(new ClickObject("Open", "Door", new RSArea(new RSTile(3084, 3128, 0), new RSTile(3088, 3123, 0))).execute())
			Timing.waitCondition(FCConditions.settingNotEqualsCondition(FCTutorial.PROGRESS_SETTING, 210), 5000);
		else
		{
			Camera.setCameraAngle(General.random(0, 100));
			Camera.setCameraRotation(General.random(0, 360));
		}
	}

	@Override
	public boolean shouldExecute()
	{
		return FCTutorial.getProgress() == 210;
	}

	@Override
	public String getStatus()
	{
		return "Open door";
	}

}

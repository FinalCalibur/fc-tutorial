package scripts.fc.missions.fctutorial.tasks.quest_guide;

import scripts.fc.api.interaction.impl.npcs.NpcDialogue;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class QuestGuideDialogue extends Task
{
	private static final long serialVersionUID = 8079259777220217093L;

	@Override
	public void execute()
	{
		new NpcDialogue("Talk-to", "Quest Guide", 15, 0).execute();

	}

	@Override
	public boolean shouldExecute()
	{
		return FCTutorial.getProgress() == 220 || FCTutorial.getProgress() == 240;
	}

	@Override
	public String getStatus()
	{
		return "Quest Guide dialogue";
	}

}

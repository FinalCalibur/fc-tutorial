package scripts.fc.missions.fctutorial.tasks.master_chef;

import org.tribot.api.Timing;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.objects.ClickObject;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class MasterChefDoor extends Task
{
	private static final long serialVersionUID = -1221964821671959304L;

	@Override
	public void execute()
	{
		if(new ClickObject("Open", "Door", new RSArea(new RSTile(3077, 3086, 0), new RSTile(3080, 3083, 0))).execute())
			Timing.waitCondition(FCConditions.settingNotEqualsCondition(FCTutorial.PROGRESS_SETTING, 130), 7500);
	}

	@Override
	public boolean shouldExecute()
	{
		return FCTutorial.getProgress() == 130;
	}

	@Override
	public String getStatus()
	{
		return "Open door";
	}

}

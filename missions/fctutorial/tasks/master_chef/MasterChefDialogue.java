package scripts.fc.missions.fctutorial.tasks.master_chef;

import scripts.fc.api.interaction.impl.npcs.NpcDialogue;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class MasterChefDialogue extends Task
{
	private static final long serialVersionUID = 1067735338811725099L;

	@Override
	public void execute()
	{
		new NpcDialogue("Talk-to", "Master Chef", 10, 0).execute();
	}

	@Override
	public boolean shouldExecute()
	{
		return FCTutorial.getProgress() == 140;
	}

	@Override
	public String getStatus()
	{
		return "Master Chef dialogue";
	}

}

package scripts.fc.missions.fctutorial.tasks.survival_expert;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.interfaces.Positionable;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Objects;
import org.tribot.api2007.PathFinding;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import org.tribot.api2007.ext.Filters;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.items.ItemUtils;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class LightFire extends Task
{
	private static final long serialVersionUID = 8317087202347142705L;
	
	@Override
	public void execute()
	{
		makeFire();
	}

	@Override
	public boolean shouldExecute()
	{
		return FCTutorial.getProgress() == 50 && Inventory.getCount("Logs") > 0;
	}

	@Override
	public String getStatus()
	{
		return "Light fire";
	}
	
	private boolean makeFire()
	{
		RSTile fireTile = new RSTile(3103, 3096, 0).translate(General.random(-2, 2), General.random(-2, 2));
		if(Player.getPosition().distanceTo(fireTile) > 3)
		{
			Camera.turnToTile(fireTile);
			if(Walking.walkScreenPath(Walking.generateStraightScreenPath(fireTile)))
				Timing.waitCondition(FCConditions.positionEquals(fireTile), 3500);
		}
		if(isFireOnTile(Player.getPosition()))
			moveToOpenTile();
		
		if(ItemUtils.useItemOnItem("Tinderbox", "Logs") && Timing.waitCondition(FCConditions.animationChanged(-1), 3000)
				&& Timing.waitCondition(FCConditions.animationChanged(733), 7500))
			return true;
		
		return false;
	}
	
	private boolean isFireOnTile(Positionable p)
	{
		return Objects.getAt(p, Filters.Objects.nameEquals("Fire")).length > 0;
	}
	
	private void moveToOpenTile()
	{
		RSArea potential = new RSArea(Player.getPosition().translate(-3, 3),
				Player.getPosition().translate(3, -3));
		
		for(RSTile t : potential.getAllTiles())
		{
			if(PathFinding.canReach(t, false) && !isFireOnTile(t))
			{
				Walking.walkTo(t);
				break;
			}
		}
	}

}

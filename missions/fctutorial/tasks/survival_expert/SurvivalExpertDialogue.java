package scripts.fc.missions.fctutorial.tasks.survival_expert;

import org.tribot.api2007.Game;
import org.tribot.api2007.Options;

import scripts.fc.api.interaction.impl.npcs.NpcDialogue;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class SurvivalExpertDialogue extends Task
{
	private static final long serialVersionUID = 7533640471665342842L;

	@Override
	public void execute()
	{
		if(!Game.isRunOn())
			Options.setRunOn(true);
		
		new NpcDialogue("Talk-to", "Survival Expert", 30, 0).execute();
	}

	@Override
	public boolean shouldExecute()
	{
		return FCTutorial.getProgress() == 20 || FCTutorial.getProgress() == 70;
	}

	@Override
	public String getStatus()
	{
		return "Survival Expert dialogue";
	}

}

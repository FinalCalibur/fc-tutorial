package scripts.fc.missions.fctutorial.tasks.survival_expert;

import org.tribot.api.Timing;
import org.tribot.api2007.Inventory;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.objects.ClickObject;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class ChopTree extends Task
{
	private static final long serialVersionUID = -2656198936560507724L;

	@Override
	public void execute()
	{
		if(new ClickObject("Chop down", "Tree", 15, true, true).execute() && Timing.waitCondition(FCConditions.animationChanged(-1), 5000))
			Timing.waitCondition(FCConditions.settingNotEqualsCondition(FCTutorial.PROGRESS_SETTING, 40), 7500);
	}

	@Override
	public boolean shouldExecute()
	{
		return FCTutorial.getProgress() == 40 || (FCTutorial.getProgress() == 50 && Inventory.getCount("Logs") == 0);
	}

	@Override
	public String getStatus()
	{
		return "Chop tree";
	}

}

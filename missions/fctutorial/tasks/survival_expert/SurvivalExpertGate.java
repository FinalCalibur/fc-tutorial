package scripts.fc.missions.fctutorial.tasks.survival_expert;

import org.tribot.api.Timing;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.objects.ClickObject;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class SurvivalExpertGate extends Task
{
	private static final long serialVersionUID = -4737101560208497031L;

	@Override
	public void execute()
	{
		if(new ClickObject("Open", "Gate", 20).execute())
			Timing.waitCondition(FCConditions.settingNotEqualsCondition(FCTutorial.PROGRESS_SETTING, 120), 6000);
	}

	@Override
	public boolean shouldExecute()
	{
		return FCTutorial.getProgress() == 120;
	}

	@Override
	public String getStatus()
	{
		return "Click gate";
	}

}

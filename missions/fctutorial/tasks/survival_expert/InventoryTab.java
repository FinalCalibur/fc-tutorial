package scripts.fc.missions.fctutorial.tasks.survival_expert;

import org.tribot.api.input.Mouse;

import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class InventoryTab extends Task
{
	private static final long serialVersionUID = -4786621914246842238L;

	@Override
	public void execute()
	{
		Mouse.clickBox(632, 170, 651, 192, 1);
	}

	@Override
	public boolean shouldExecute()
	{
		return FCTutorial.getProgress() == 30;
	}

	@Override
	public String getStatus()
	{
		return "Inventory tab";
	}

}

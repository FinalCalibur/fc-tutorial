package scripts.fc.missions.fctutorial.tasks.survival_expert;

import org.tribot.api.Timing;
import org.tribot.api2007.Inventory;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.objects.ItemOnObject;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class CookShrimps extends Task
{
	private static final long serialVersionUID = 6159860992107728108L;

	@Override
	public void execute()
	{
		cookShrimps();
	}

	@Override
	public boolean shouldExecute()
	{
		final int SETTING = FCTutorial.getProgress();
		final int SHRIMPS = Inventory.getCount("Raw shrimps");
		return (SETTING == 90 && SHRIMPS >= 2) || (SETTING == 110 && SHRIMPS >= 1);
	}

	@Override
	public String getStatus()
	{
		return "Cook shrimps";
	}
	
	
	private void cookShrimps()
	{
		if(new ItemOnObject("null", "Fire", "Raw shrimps", 15).execute())
			if(Timing.waitCondition(FCConditions.animationChanged(-1), 6500))
				Timing.waitCondition(FCConditions.animationChanged(897), 6500);
	}

}

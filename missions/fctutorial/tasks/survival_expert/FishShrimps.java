package scripts.fc.missions.fctutorial.tasks.survival_expert;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Inventory;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.npcs.ClickNpc;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class FishShrimps extends Task
{
	private static final long serialVersionUID = -6769017398897731224L;

	@Override
	public void execute()
	{
		fishShrimps();
	}

	@Override
	public boolean shouldExecute()
	{
		return FCTutorial.getProgress() == 80 || (FCTutorial.getProgress() == 90 && Inventory.getCount("Raw shrimps") == 1);
	}

	@Override
	public String getStatus()
	{
		return "Fish shrimps";
	}
	
	private void fishShrimps()
	{	
		if(new ClickNpc("Net", "Fishing spot", 15).execute())
			Timing.waitCondition(FCConditions.inventoryChanged(Inventory.getAll().length), General.random(6000, 7500));
	}
	
}

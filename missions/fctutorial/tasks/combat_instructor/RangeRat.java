package scripts.fc.missions.fctutorial.tasks.combat_instructor;

import org.tribot.api.Timing;
import org.tribot.api2007.Equipment;
import org.tribot.api2007.Player;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.npcs.AttackNpc;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class RangeRat extends Task
{
	private static final long serialVersionUID = -6634211527227299511L;

	@Override
	public void execute()
	{
		AttackNpc attack = new AttackNpc("Attack", "Giant rat", 15);
		attack.setCheckPath(false);
		
		if(attack.execute() && Timing.waitCondition(FCConditions.IN_COMBAT_CONDITION, 3000))
			Timing.waitCondition(FCConditions.KILL_CONDITION, 30000);
	}

	@Override
	public boolean shouldExecute()
	{
		return (FCTutorial.getProgress() == 480 && Equipment.isEquipped("Bronze arrow") && Equipment.isEquipped("Shortbow"))
					|| (FCTutorial.getProgress() == 490 && Player.getRSPlayer().getInteractingCharacter() == null);
	}

	@Override
	public String getStatus()
	{
		return "Range rat";
	}

}

package scripts.fc.missions.fctutorial.tasks.combat_instructor;

import org.tribot.api.Timing;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.objects.ClickObject;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class EnterRatCage extends Task
{
	private static final long serialVersionUID = 5780492008028034953L;

	@Override
	public void execute()
	{
		if(new ClickObject("Open", "Gate", new RSArea(new RSTile(3108, 9521, 0), new RSTile(3112, 9516, 0))).execute())
			Timing.waitCondition(FCConditions.settingNotEqualsCondition(FCTutorial.PROGRESS_SETTING, 440), 7500);
	}

	@Override
	public boolean shouldExecute()
	{
		return FCTutorial.getProgress() == 440;
	}

	@Override
	public String getStatus()
	{
		return "Enter rat cage";
	}

}

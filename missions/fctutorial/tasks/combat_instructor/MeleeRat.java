package scripts.fc.missions.fctutorial.tasks.combat_instructor;

import org.tribot.api.Timing;
import org.tribot.api2007.Player;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.npcs.AttackNpc;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class MeleeRat extends Task
{
	private static final long serialVersionUID = -4826359909960013294L;
	
	int setting;

	@Override
	public void execute()
	{
		if(new AttackNpc("Attack", "Giant rat", 15).execute() && Timing.waitCondition(FCConditions.IN_COMBAT_CONDITION, 3500)
				&& Timing.waitCondition(FCConditions.KILL_CONDITION, 40000))
			Timing.waitCondition(FCConditions.settingNotEqualsCondition(460, setting), 40000);
	}

	@Override
	public boolean shouldExecute()
	{
		setting = FCTutorial.getProgress();
		return setting == 450 || (setting == 460 && !Player.getRSPlayer().isInCombat());
	}

	@Override
	public String getStatus()
	{
		return "Melee rat";
	}

}

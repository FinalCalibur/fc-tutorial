package scripts.fc.missions.fctutorial.tasks.financial_advisor;

import scripts.fc.api.interaction.impl.npcs.NpcDialogue;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class FinancialAdvisorDialogue extends Task
{
	private static final long serialVersionUID = 6700614689780576513L;

	@Override
	public void execute()
	{
		new NpcDialogue("Talk-to", "Financial Advisor", 15, 0).execute();
	}

	@Override
	public boolean shouldExecute()
	{
		return FCTutorial.getProgress() == 530;
	}

	@Override
	public String getStatus()
	{
		return "Financial Advisor Dialogue";
	}

}

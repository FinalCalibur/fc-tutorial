package scripts.fc.missions.fctutorial.tasks.prayer_guide;

import org.tribot.api2007.Player;

import scripts.fc.api.interaction.impl.npcs.NpcDialogue;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class BrotherBraceDialogue extends Task
{
	private static final long serialVersionUID = 3105566098270435996L;

	@Override
	public void execute()
	{ 
		new NpcDialogue("Talk-to", "Brother Brace", 15, 0).execute();
	}

	@Override
	public boolean shouldExecute()
	{
		final int PROGRESS = FCTutorial.getProgress();
		return (PROGRESS == 550 && Player.getPosition().distanceTo(WalkToBrotherBrace.TARGET_TILE) <= WalkToBrotherBrace.DISTANCE_THRESHOLD) 
					|| PROGRESS == 570 || PROGRESS == 600; 
	}

	@Override
	public String getStatus()
	{
		return "Brother Brace dialogue";
	}

}

package scripts.fc.missions.fctutorial.tasks.magic_instructor;

import org.tribot.api2007.Player;

import scripts.fc.api.interaction.impl.npcs.NpcDialogue;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class MagicInstructorDialogue extends Task
{
	private static final long serialVersionUID = -2871454659516233306L;

	@Override
	public void execute()
	{
		new NpcDialogue("Talk-to", "Magic Instructor", 30, 0).execute();
	}

	@Override
	public boolean shouldExecute()
	{
		final int SETTING = FCTutorial.getProgress();
		return (SETTING == 620 && Player.getPosition().distanceTo(WalkToMagicInstructor.TARGET) <= WalkToMagicInstructor.THRESHOLD) 
					|| SETTING == 640 || SETTING == 670;
	}

	@Override
	public String getStatus()
	{
		return "Magic Instructor dialogue";
	}

}

package scripts.fc.missions.fctutorial.tasks.mining_instructor;

import org.tribot.api.Clicking;
import org.tribot.api.Timing;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.types.RSInterface;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class MakeDagger extends Task
{
	private static final long serialVersionUID = 3960209709548589124L;
	private static final int INTER_CHILD = 2;
	
	private RSInterface master;

	@Override
	public void execute()
	{
		RSInterface daggerChild = master.getChild(INTER_CHILD);
		if(daggerChild != null && Clicking.click(daggerChild) && Timing.waitCondition(FCConditions.animationChanged(-1), 5000))
			Timing.waitCondition(FCConditions.animationChanged(898), 5000);
	}

	@Override
	public boolean shouldExecute()
	{
		master = Interfaces.get(ClickAnvil.INTERFACE_MASTER);
		return FCTutorial.getProgress() == 350 && master != null;
	}

	@Override
	public String getStatus()
	{
		return "Make dagger";
	}

}

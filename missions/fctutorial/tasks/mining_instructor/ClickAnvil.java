package scripts.fc.missions.fctutorial.tasks.mining_instructor;

import org.tribot.api.Timing;
import org.tribot.api2007.Interfaces;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.objects.ClickObject;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class ClickAnvil extends Task
{
	private static final long serialVersionUID = 8149746082672492751L;
	
	public static final int INTERFACE_MASTER = 312;

	@Override
	public void execute()
	{
		if(new ClickObject("Smith", "Anvil", 25).execute())
			Timing.waitCondition(FCConditions.interfaceUp(INTERFACE_MASTER), 5000);
	}

	@Override
	public boolean shouldExecute()
	{
		return FCTutorial.getProgress() == 340 || (FCTutorial.getProgress() == 350 && Interfaces.get(INTERFACE_MASTER) == null);
	}

	@Override
	public String getStatus()
	{
		return "Click anvil";
	}

}

package scripts.fc.missions.fctutorial.tasks.mining_instructor;

import org.tribot.api.interfaces.Positionable;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSTile;
import org.tribot.api2007.util.DPathNavigator;

import scripts.fc.api.interaction.impl.npcs.NpcDialogue;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class MiningInstructorDialogue extends Task
{
	private static final long serialVersionUID = 6132602685767319522L;

	private static final Positionable TARGET_TILE = new RSTile(3081, 9507, 0);
	private static final int DISTANCE_THRESHOLD = 8;
	
	@Override
	public void execute()
	{
		if(Player.getPosition().distanceTo(TARGET_TILE) > DISTANCE_THRESHOLD)
			new DPathNavigator().traverse(TARGET_TILE);
		else
			new NpcDialogue("Talk-to", "Mining Instructor", 15, 2, 2).execute();
	}

	@Override
	public boolean shouldExecute()
	{
		final int SETTING = FCTutorial.getProgress();
		return SETTING == 260 || SETTING == 290 || SETTING == 330;
	}

	@Override
	public String getStatus()
	{
		return "Mining Instructor dialogue";
	}

}

package scripts.fc.missions.fctutorial.tasks.mining_instructor;

import org.tribot.api.Timing;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import org.tribot.api2007.types.RSTile;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.objects.ItemOnObject;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class SmeltOre extends Task
{
	private static final long serialVersionUID = -8822531871057467898L;

	@Override
	public void execute()
	{
		smeltOre();
	}

	@Override
	public boolean shouldExecute()
	{
		return FCTutorial.getProgress() == 320;
	}

	@Override
	public String getStatus()
	{
		return "Smelt ore";
	}
	
	private void smeltOre()
	{
		RSTile targetTile = new RSTile(3079, 9497, 0);
		Camera.turnToTile(targetTile);
		if(!Player.getPosition().equals(targetTile))
		{
			if(!Walking.walkScreenPath(Walking.generateStraightScreenPath(targetTile)))
			{
				if(Walking.blindWalkTo(targetTile))
					Timing.waitCondition(FCConditions.positionEquals(targetTile), 5000);
			}
			else
				Timing.waitCondition(FCConditions.positionEquals(targetTile), 5000);
		}
		else
		{
			if(new ItemOnObject(null, 10082, "Tin ore", 20).execute() && Timing.waitCondition(FCConditions.inventoryChanged(Inventory.getAll().length), 10000))
				Timing.waitCondition(FCConditions.animationChanged(899), 5000);
		}
	}

}

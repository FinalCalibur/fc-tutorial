package scripts.fc.missions.fctutorial.tasks.mining_instructor;

import org.tribot.api.Timing;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.objects.ClickObject;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class MiningInstructorExit extends Task
{
	private static final long serialVersionUID = -6631125048389989843L;

	@Override
	public void execute()
	{
		if(new ClickObject("Open", "Gate", 25).execute())
			Timing.waitCondition(FCConditions.settingNotEqualsCondition(FCTutorial.PROGRESS_SETTING, 360), 6000);
	}

	@Override
	public boolean shouldExecute()
	{
		return FCTutorial.getProgress() == 360;
	}

	@Override
	public String getStatus()
	{
		return "Click gate";
	}

}

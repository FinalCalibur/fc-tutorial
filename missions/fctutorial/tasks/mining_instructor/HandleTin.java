package scripts.fc.missions.fctutorial.tasks.mining_instructor;

import org.tribot.api.Timing;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.objects.ClickObject;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class HandleTin extends Task
{
	private static final long serialVersionUID = -4014208023321206656L;
	
	private int setting;

	@Override
	public void execute()
	{
		if(new ClickObject((setting == 270 ? "Prospect" : "Mine"), "Rocks", new RSArea(new RSTile(3072, 9512, 0), new RSTile(3078, 9500, 0))).execute())
			Timing.waitCondition(FCConditions.settingNotEqualsCondition(FCTutorial.PROGRESS_SETTING, setting), 8000);
	}

	@Override
	public boolean shouldExecute()
	{
		setting = FCTutorial.getProgress();
		return setting == 270 || setting == 300;
	}

	@Override
	public String getStatus()
	{
		return setting == 270 ? "Prospect tin" : "Mine tin";
	}

}

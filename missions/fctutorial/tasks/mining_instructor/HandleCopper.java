package scripts.fc.missions.fctutorial.tasks.mining_instructor;

import org.tribot.api.Timing;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.objects.ClickObject;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class HandleCopper extends Task
{
	private static final long serialVersionUID = 297434712529988827L;
	
	private int setting;

	@Override
	public void execute()
	{
		if(new ClickObject((setting == 280 ? "Prospect" : "Mine"), "Rocks", new RSArea(new RSTile(3081, 9505, 0), new RSTile(3087, 9495, 0))).execute())
			Timing.waitCondition(FCConditions.settingNotEqualsCondition(FCTutorial.PROGRESS_SETTING, setting), 6000);
	}

	@Override
	public boolean shouldExecute()
	{
		setting = FCTutorial.getProgress();
		return setting == 280 || setting == 310;
	}

	@Override
	public String getStatus()
	{
		return setting == 280 ? "Prospect copper" : "Mine copper";
	}

}

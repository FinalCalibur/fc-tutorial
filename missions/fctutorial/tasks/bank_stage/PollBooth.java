package scripts.fc.missions.fctutorial.tasks.bank_stage;

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.NPCChat;
import org.tribot.api2007.types.RSInterface;

import scripts.fc.api.generic.FCConditions;
import scripts.fc.api.interaction.impl.objects.ClickObject;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.FCTutorial;

public class PollBooth extends Task
{
	private static final long serialVersionUID = 1240933668596100934L;
	private static final int POLL_BOOTH_MASTER = 310;
	private static final int POLL_BOOTH_CLOSE_CHILD = 1;
	private static final int POLL_BOOTH_CLOSE_COMP = 11;
	

	@Override
	public void execute()
	{
		if(Banking.close() && new ClickObject("Use", "Poll booth", 15).execute() && Timing.waitCondition(FCConditions.IN_DIALOGUE_CONDITION, 3500))
		{
			RSInterface pollBooth;
			
			while((pollBooth = Interfaces.get(POLL_BOOTH_MASTER)) == null && NPCChat.clickContinue(false))
				General.sleep(100, 450);
			
			RSInterface closeButton = pollBooth == null ? null : pollBooth.getChild(POLL_BOOTH_CLOSE_CHILD);
			RSInterface closeComp = closeButton == null ? null : closeButton.getChild(POLL_BOOTH_CLOSE_COMP);
			
			while(closeComp != null && (!Clicking.click(closeComp) || !Timing.waitCondition(FCConditions.interfaceUp(POLL_BOOTH_MASTER), 2000)))
			{
				pollBooth = Interfaces.get(POLL_BOOTH_MASTER);
				closeButton = pollBooth == null ? null : pollBooth.getChild(POLL_BOOTH_CLOSE_CHILD);
				closeComp = closeButton == null ? null : closeButton.getChild(POLL_BOOTH_CLOSE_COMP);
				General.sleep(100, 450);
			}
			
		}
	}

	@Override
	public boolean shouldExecute()
	{
		return FCTutorial.getProgress() == 520; 
	}

	@Override
	public String getStatus()
	{
		return "Poll booth";
	}

}

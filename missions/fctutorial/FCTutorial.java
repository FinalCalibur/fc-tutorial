package scripts.fc.missions.fctutorial;

import java.util.Arrays;
import java.util.LinkedList;

import org.tribot.api2007.Game;

import scripts.fc.framework.goal.GoalList;
import scripts.fc.framework.mission.Mission;
import scripts.fc.framework.mission.MissionManager;
import scripts.fc.framework.script.FCMissionScript;
import scripts.fc.framework.task.Task;
import scripts.fc.missions.fctutorial.tasks.GeneralChecks;
import scripts.fc.missions.fctutorial.tasks.StuckChecks;
import scripts.fc.missions.fctutorial.tasks.bank_stage.BankerDialogue;
import scripts.fc.missions.fctutorial.tasks.bank_stage.BankingExit;
import scripts.fc.missions.fctutorial.tasks.bank_stage.PollBooth;
import scripts.fc.missions.fctutorial.tasks.combat_instructor.CombatInstructorDialogue;
import scripts.fc.missions.fctutorial.tasks.combat_instructor.CombatInstructorExit;
import scripts.fc.missions.fctutorial.tasks.combat_instructor.CombatTab;
import scripts.fc.missions.fctutorial.tasks.combat_instructor.EnterRatCage;
import scripts.fc.missions.fctutorial.tasks.combat_instructor.EquipBowAndArrow;
import scripts.fc.missions.fctutorial.tasks.combat_instructor.EquipSwordAndShield;
import scripts.fc.missions.fctutorial.tasks.combat_instructor.EquipmentTab;
import scripts.fc.missions.fctutorial.tasks.combat_instructor.MeleeRat;
import scripts.fc.missions.fctutorial.tasks.combat_instructor.RangeRat;
import scripts.fc.missions.fctutorial.tasks.combat_instructor.ViewEquipment;
import scripts.fc.missions.fctutorial.tasks.combat_instructor.WieldDagger;
import scripts.fc.missions.fctutorial.tasks.financial_advisor.FinancialAdvisorDialogue;
import scripts.fc.missions.fctutorial.tasks.financial_advisor.FinancialAdvisorExit;
import scripts.fc.missions.fctutorial.tasks.magic_instructor.CastWindStrike;
import scripts.fc.missions.fctutorial.tasks.magic_instructor.MagicInstructorDialogue;
import scripts.fc.missions.fctutorial.tasks.magic_instructor.MagicTab;
import scripts.fc.missions.fctutorial.tasks.magic_instructor.WalkToMagicInstructor;
import scripts.fc.missions.fctutorial.tasks.master_chef.CookDough;
import scripts.fc.missions.fctutorial.tasks.master_chef.MakeDough;
import scripts.fc.missions.fctutorial.tasks.master_chef.MasterChefDialogue;
import scripts.fc.missions.fctutorial.tasks.master_chef.MasterChefDoor;
import scripts.fc.missions.fctutorial.tasks.master_chef.MasterChefExit;
import scripts.fc.missions.fctutorial.tasks.master_chef.MusicTab;
import scripts.fc.missions.fctutorial.tasks.mining_instructor.ClickAnvil;
import scripts.fc.missions.fctutorial.tasks.mining_instructor.HandleCopper;
import scripts.fc.missions.fctutorial.tasks.mining_instructor.HandleTin;
import scripts.fc.missions.fctutorial.tasks.mining_instructor.MakeDagger;
import scripts.fc.missions.fctutorial.tasks.mining_instructor.MiningInstructorDialogue;
import scripts.fc.missions.fctutorial.tasks.mining_instructor.MiningInstructorExit;
import scripts.fc.missions.fctutorial.tasks.mining_instructor.SmeltOre;
import scripts.fc.missions.fctutorial.tasks.prayer_guide.BrotherBraceDialogue;
import scripts.fc.missions.fctutorial.tasks.prayer_guide.BrotherBraceExit;
import scripts.fc.missions.fctutorial.tasks.prayer_guide.FriendsTab;
import scripts.fc.missions.fctutorial.tasks.prayer_guide.IgnoreTab;
import scripts.fc.missions.fctutorial.tasks.prayer_guide.PrayerTab;
import scripts.fc.missions.fctutorial.tasks.prayer_guide.WalkToBrotherBrace;
import scripts.fc.missions.fctutorial.tasks.quest_guide.ClickEmote;
import scripts.fc.missions.fctutorial.tasks.quest_guide.ClickOptions;
import scripts.fc.missions.fctutorial.tasks.quest_guide.EmotesTab;
import scripts.fc.missions.fctutorial.tasks.quest_guide.OpenQuestGuideDoor;
import scripts.fc.missions.fctutorial.tasks.quest_guide.QuestGuideDialogue;
import scripts.fc.missions.fctutorial.tasks.quest_guide.QuestGuideLadder;
import scripts.fc.missions.fctutorial.tasks.quest_guide.QuestTab;
import scripts.fc.missions.fctutorial.tasks.quest_guide.TurnRunOn;
import scripts.fc.missions.fctutorial.tasks.runescape_guide.AccountDesign;
import scripts.fc.missions.fctutorial.tasks.runescape_guide.OptionsTab;
import scripts.fc.missions.fctutorial.tasks.runescape_guide.RunescapeGuideDialogue;
import scripts.fc.missions.fctutorial.tasks.runescape_guide.RunescapeGuideDoor;
import scripts.fc.missions.fctutorial.tasks.survival_expert.ChopTree;
import scripts.fc.missions.fctutorial.tasks.survival_expert.CookShrimps;
import scripts.fc.missions.fctutorial.tasks.survival_expert.FishShrimps;
import scripts.fc.missions.fctutorial.tasks.survival_expert.InventoryTab;
import scripts.fc.missions.fctutorial.tasks.survival_expert.LightFire;
import scripts.fc.missions.fctutorial.tasks.survival_expert.StatsTab;
import scripts.fc.missions.fctutorial.tasks.survival_expert.SurvivalExpertDialogue;
import scripts.fc.missions.fctutorial.tasks.survival_expert.SurvivalExpertGate;

public class FCTutorial extends MissionManager implements Mission
{
	public FCTutorial(FCMissionScript script)
	{
		super(script);
	}
	
	public FCTutorial(FCMissionScript script, String key)
	{
		super(script);
	}

	public static final int PROGRESS_SETTING = 281;
	
	@Override
	public boolean hasReachedEndingCondition()
	{
		return getProgress() == 1000;
	}

	@Override
	public String getMissionName()
	{
		return "Tutorial Island";
	}

	@Override
	public String getCurrentTaskName()
	{
		return currentTask == null ? "null" : currentTask.getStatus();
	}

	@Override
	public String getEndingMessage()
	{
		return "FC Tutorial has been completed";
	}

	@Override
	public GoalList getGoals()
	{
		return goals;
	}

	@Override
	public String[] getMissionSpecificPaint()
	{
		return new String[0];
	}

	@Override
	public void execute()
	{
		executeTasks();
	}

	@Override
	public LinkedList<Task> getTaskList()
	{
		return new LinkedList<>(Arrays.asList(
			new StuckChecks(), new GeneralChecks(), new AccountDesign(), new RunescapeGuideDialogue(), new OptionsTab(),
			new RunescapeGuideDoor(), new SurvivalExpertDialogue(), new InventoryTab(), new ChopTree(), new LightFire(),
			new StatsTab(), new FishShrimps(), new CookShrimps(), new SurvivalExpertGate(), new MasterChefDoor(),
			new MasterChefDialogue(), new MakeDough(), new CookDough(), new MusicTab(), new MasterChefExit(),
			new EmotesTab(), new ClickEmote(), new ClickOptions(), new TurnRunOn(), new OpenQuestGuideDoor(),
			new QuestGuideDialogue(), new QuestTab(), new QuestGuideLadder(), new MiningInstructorDialogue(),
			new HandleTin(), new HandleCopper(), new SmeltOre(), new ClickAnvil(), new MakeDagger(),
			new MiningInstructorExit(), new CombatInstructorDialogue(), new EquipmentTab(), new ViewEquipment(),
			new WieldDagger(), new EquipSwordAndShield(), new CombatTab(), new EnterRatCage(), new MeleeRat(),
			new EquipBowAndArrow(), new RangeRat(), new CombatInstructorExit(), new BankerDialogue(),
			new PollBooth(), new BankingExit(), new FinancialAdvisorDialogue(), new FinancialAdvisorExit(),
			new WalkToBrotherBrace(), new BrotherBraceDialogue(), new PrayerTab(), new FriendsTab(), new IgnoreTab(),
			new BrotherBraceExit(), new MagicInstructorDialogue(), new WalkToMagicInstructor(), new MagicTab(),
			new CastWindStrike()
			));
	}
	
	public static int getProgress()
	{
		return Game.getSetting(PROGRESS_SETTING);
	}
	
	public String toString()
	{
		return getMissionName();
	}

	@Override
	public void resetStatistics()
	{}

}
